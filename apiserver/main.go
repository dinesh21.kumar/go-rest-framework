package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
)

type post struct {
	title  string `json:"title"`
	body   string `json:"body"`
	userID int    `json:"userID"`
}

//use .env to import go environment varaibles
// init is invoked before main()
func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

func setLogging() {
	// Configure Logging
	// logFileLocation := os.Getenv("LOG_FILE_LOCATION")
	// fmt.Println(logFileLocation)
	// if logFileLocation != "" {
	// 	log.SetOutput(&lumberjack.Logger{
	// 		Filename:   logFileLocation,
	// 		MaxSize:    200, // megabytes
	// 		MaxBackups: 3,
	// 		MaxAge:     28,   //days
	// 		Compress:   true, // disabled by default
	// 	})
	// }
	// log.Print("Logger initiated") //will print log in ./app.log file

	logFileLocation := os.Getenv("LOG_FILE_LOCATION")
	file, e := os.OpenFile(logFileLocation, os.O_APPEND|os.O_WRONLY, 0644)
	if e != nil {
		logrus.Error(e.Error())
	}
	logrus.SetFormatter(&logrus.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})

	//add function from which log was generated
	logrus.SetReportCaller(true)
	logrus.SetOutput(file)
	logrus.Info("logger has started, yayy")
}

func main() {
	fmt.Println("This will create a go server to handle GET,POST,PUT,DELETE requests")
	setLogging()
	mux := newRouter()
	error := http.ListenAndServe(":8080", mux)
	if error != nil {
		logrus.Error(error.Error())
		fmt.Println("error while listen & serve listen ", error)
	} else {
		logrus.Info("Go server has started listening on localhost:8080")
		fmt.Println("Go server has started listening on localhost:8080")
	}
}

func welcomeMessage(res http.ResponseWriter, req *http.Request) {
	fmt.Println("Hey Geek, Welcome to Go Lang")
	req.ParseForm()
	name := req.Form.Get("name")
	res.Header().Set("Content-Type", "text/plain")
	res.Header().Set("Server Info", "Go server")
	res.WriteHeader(200)
	msg := ""
	if name != "" {
		msg = "Hey " + name + " Welcome to Go Language"
	} else {
		msg = "Hey Dude Welcome to Go Language"
	}
	res.Write([]byte(msg))
}

func getPosts(res http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		log.Panicf("does not support POST method")
	}
	res.Header().Set("Content-Type", "application/json")
	req.ParseForm()
	fmt.Println("request Form values  = ", req.Form)
	postID := req.Form.Get("post_id")
	fmt.Println("post id   = ", postID)
	url := "https://jsonplaceholder.typicode.com/posts"
	if postID != "" {
		url = url + "/" + string(postID)
	}
	resp, error := http.Get(url)
	if error != nil {
		logrus.Error(error.Error())
	}
	fmt.Println("response from posts api is = ")
	fmt.Println(resp)
	fmt.Println("response body from posts api is = ")
	fmt.Println(resp.Body)
	fmt.Printf("Type of response body is %T = ", resp.Body)

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.Error(error.Error())
	} else {
		//fmt.Println("body after converting to []byte slice ", bodyBytes)
		res.Write(bodyBytes)
	}
}

func createPost(res http.ResponseWriter, req *http.Request) {
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		logrus.Error(err.Error())
	}
	//var p post
	//keyVal := make(map[string]string)
	//store post body in a map
	keyVal := map[string]interface{}{}
	json.Unmarshal(b, &keyVal)

	title := keyVal["title"]
	body := keyVal["body"]
	userID := keyVal["userID"]
	fmt.Println("\ntitle = ", title)
	fmt.Println("body = ", body)
	fmt.Println("userID = ", userID)
	newpost := map[string]interface{}{
		"title":  title,
		"body":   body,
		"userID": userID,
	}
	bytesRepresentation, err := json.Marshal(newpost)
	resp, err := http.Post("https://jsonplaceholder.typicode.com/posts", "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		logrus.Error(err.Error())
	}
	defer resp.Body.Close()
	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)

	log.Println(result)
	r, _ := json.Marshal(result)
	res.Header().Set("Content-Type", "application/json")
	res.Write(r)
}

func employees(res http.ResponseWriter, req *http.Request) {
	list := getEmployees()
	fmt.Println("employee list slice in main = ", list)
	employeeListJSON, err := json.Marshal(list)
	if err != nil {
		logrus.Error(err.Error())
	}
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(200)
	res.Write(employeeListJSON)
}

func addEmployee(res http.ResponseWriter, req *http.Request) {
	body := req.Body
	keymap := map[string]interface{}{}
	b, e := ioutil.ReadAll(body)
	if e != nil {
		logrus.Error(e.Error())
	}
	json.Unmarshal(b, &keymap)

	fmt.Println("add employee body is = ", keymap)
	fmt.Printf("%T = ", keymap)
	output := createEmployee(keymap)
	res.Header().Set("Content-Type", "application/text")
	res.WriteHeader(201)
	res.Write([]byte(output))
}

func removeEmployee(res http.ResponseWriter, req *http.Request) {
	body := req.Body
	keymap := map[string]interface{}{}
	b, e := ioutil.ReadAll(body)
	if e != nil {
		logrus.Error(e.Error())
	}
	json.Unmarshal(b, &keymap)

	fmt.Println("remove employee body is = ", keymap)
	fmt.Printf("%T = ", keymap)
	output := deleteEmployee(keymap)
	res.Header().Set("Content-Type", "application/text")
	res.WriteHeader(201)
	res.Write([]byte(output))
}

func changeEmployeeLastName(res http.ResponseWriter, req *http.Request) {
	body := req.Body
	keymap := map[string]interface{}{}
	b, e := ioutil.ReadAll(body)
	if e != nil {
		logrus.Error(e.Error())
	}
	json.Unmarshal(b, &keymap)

	fmt.Println("update employee body is = ", keymap)
	fmt.Printf("%T = ", keymap)
	output := updateEmployee(keymap)
	res.Header().Set("Content-Type", "application/text")
	res.WriteHeader(201)
	res.Write([]byte(output))
}

func exampleV1Handl(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/text")
	res.WriteHeader(200)
	res.Write([]byte("this is example version v1 response"))
}

func exampleV2Handl(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/text")
	res.WriteHeader(200)
	res.Write([]byte("this is example version v2 response"))
}

//call multiple api in parallel using go routines and channels
func asyncAirlineStatus(res http.ResponseWriter, req *http.Request) {

	var urls = []string{
		"https://www.easyjet.com/",
		"https://www.skyscanner.de/",
		"https://www.ryanair.com",
		"https://wizzair.com/",
		"https://www.swiss.com/",
	}

	//lets create a channel on which checkURL will write url status
	channel := make(chan urlStatus) // urlstatus type will be written on channel
	//create wait group
	for index, url := range urls {
		fmt.Println(index, url)
		go checkURL(url, channel) //use go keyword for run this function parrallel for each url
	}

	//read values from channel
	result := make([]urlStatus, len(urls))

	for i := 0; i < len(urls); i++ {
		result[i] = <-channel
		if result[i].status {
			fmt.Println("url ", result[i].url, " is active")
		} else {
			fmt.Println("url ", result[i].url, " is inactive")
		}

	}

	//res.Write(result)
	jsonRep, e := json.Marshal(res)
	if e != nil {
		logrus.Error(e.Error())
	}
	res.Write(jsonRep)
}

func checkURL(u string, channel chan urlStatus) {
	_, err := http.Get(u)
	if err != nil {
		//fmt.Println("url ", u, " is inactive")
		channel <- urlStatus{status: false, url: u}
	} else {
		//fmt.Println("url ", u, " is active")
		channel <- urlStatus{status: true, url: u}
	}
}

type urlStatus struct {
	status bool
	url    string
}

//used for test cases
func sum(nums ...int) int {
	total := 0
	for _, num := range nums {
		total += num
	}
	return total
}

//create a basic function which will call a http endpoint

func isArticleValid() bool {
	resp, err := http.Get("https://api.mybiz.com/articles")
	output := true
	var articles []Article
	if err != nil {
		logrus.Error(err.Error())
	} else {
		fmt.Println("called HTTP called & resp= ", resp)
		fmt.Printf("\n type of resp = %T\n", resp)
		body := resp.Body
		byres, _ := ioutil.ReadAll(body) //convert string body to []byte
		fmt.Sprintln(string(byres))
		e := json.Unmarshal(byres, &articles) //as body is list of articles,convert []byte to []Article
		if e != nil {
			logrus.Error(e.Error())
		}
	}
	//an article is valid if id is present
	for _, v := range articles {
		if v.ID == 0 {
			return false
		}
	}
	return output
}

type Article struct {
	ID   int    `json:"id"`   // Uppercased first letter
	Name string `json:"name"` // Uppercased first letter
}
