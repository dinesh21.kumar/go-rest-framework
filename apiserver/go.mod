module gitlab.com/dinesh21.kumar/apiserver

go 1.13

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.3
	github.com/jarcoal/httpmock v1.0.4
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.4.2
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
