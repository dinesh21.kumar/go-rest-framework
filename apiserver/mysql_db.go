package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/sirupsen/logrus"

	_ "github.com/go-sql-driver/mysql"
)

//test comment
type Employee struct {
	ID        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	JobTitle  string `json:"job_title"`
	Salary    int    `json:"salary"`
}

func init() {
	setLogging()
}

//will be used by others
func getConnection() *sql.DB {
	//username,password,database,ip,port name has to be moved to env
	username, _ := os.LookupEnv("MYSQL_USER_NAME")
	password, _ := os.LookupEnv("MYSQL_USER_PASSWORD")
	ip, _ := os.LookupEnv("MYSQL_HOST_IP")
	port, _ := os.LookupEnv("MYSQL_PORT")
	dbname, _ := os.LookupEnv("MYSQL_DB_NAME")

	connectionURL := username + password + ":@tcp" + "(" + ip + ":" + port + ")/" + dbname
	fmt.Println("Connection URL is = ", connectionURL)
	db, err := sql.Open("mysql",
		connectionURL)
	if err != nil {
		fmt.Println("connection error")
		//log.Fatal(err)
		logrus.Error(err.Error())
		return nil
	}
	//defer db.Close()
	return db
}

//return all records
func getEmployees() []Employee {
	db := getConnection()
	defer db.Close()
	rows, err := db.Query("select id,first_name,last_name,job_title,salary from employee")
	if err != nil {
		fmt.Println("error while reading all employee rows", err.Error())
		log.Fatal(err)
	}
	defer rows.Close()
	list := make([]Employee, 0, 20)
	for rows.Next() {
		emp := Employee{}
		e := rows.Scan(&emp.ID, &emp.FirstName, &emp.LastName, &emp.JobTitle, &emp.Salary)
		if e != nil {
			fmt.Println("error while scanning rows = ", e.Error())
			logrus.Error(e.Error())
		}
		fmt.Println("employee = ", emp)
		list = append(list, emp)
	}
	fmt.Println("employee list slice = ", list)
	logrus.Info("connection estabilshed to mysql")
	return list
}

//Create a new employee
func createEmployee(keymap map[string]interface{}) string {
	db := getConnection()
	id := keymap["id"]
	firstName := keymap["first_name"]
	lastName := keymap["last_name"]
	jobTitle := keymap["job_title"]
	salary := keymap["salary"]

	psmt, e := db.Prepare("INSERT INTO employee VALUES( ?, ? ,?,?,?,?)")
	defer psmt.Close()
	if e != nil {
		logrus.Error(e.Error())
	}
	res, err := psmt.Exec(id, firstName, lastName, jobTitle, salary, nil)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("inserted sucessfully", res)
	defer db.Close()
	return "inserted sucessfully"
}

//delete a  employee by id
func deleteEmployee(keymap map[string]interface{}) string {
	db := getConnection()
	id := keymap["id"]
	psmt, e := db.Prepare("delete from  employee where id = ?")
	defer psmt.Close()
	if e != nil {
		logrus.Error(e.Error())
	}
	res, err := psmt.Exec(id)
	if err != nil {
		logrus.Error(err.Error())
	}
	fmt.Println("deleted sucessfully", res)
	defer db.Close()
	return "deleted sucessfully"
}

//update a  employee by id, replace old lastname to new lastname
func updateEmployee(keymap map[string]interface{}) string {
	db := getConnection()
	id := keymap["id"]
	newLastName := keymap["new_last_name"]
	psmt, e := db.Prepare("update employee  set last_name = ?  where id = ?")
	defer psmt.Close()
	if e != nil {
		logrus.Error(e.Error())
	}
	res, err := psmt.Exec(newLastName, id)
	if err != nil {
		logrus.Error(err.Error())
	}
	fmt.Println("updated sucessfully", res)
	defer db.Close()
	return "updated sucessfully"
}
