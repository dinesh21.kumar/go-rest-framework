package main

import (
	"fmt"
	"testing"

	"github.com/jarcoal/httpmock"
)

//commands used to run unit test cases
//1. go test -v
//2. to get success/failure lines in green and red use  #go get -u github.com/rakyll/gotest
//gotest -v

//3. get code coverage,  # go test -cover
//4. get coverage in txt file # go test -coverprofile=cover.txt

//5.get coverage in html from cover.txt in step4 format # go tool cover -html=cover.txt -o cover.html

func TestSum(t *testing.T) {
	nums := []int{1, 2, 3, 4}
	result := sum(nums...) //pass values of num

	if result == 10 {
		t.Logf("Test sum passed expected %v, got %v", 10, result)
	} else {
		t.Errorf("Test sum failed expected %v, got %v", 10, result)
	}
}

func TestSumWithEmptyArr(t *testing.T) {
	nums := []int{}
	result := sum(nums...) //pass values of num

	if result == 0 {
		t.Logf("Test sum passed expected %v, got %v", 0, result)
	} else {
		t.Errorf("Test sum failed expected %v, got %v", 0, result)
	}
}

//test http mock using import "github.com/jarcoal/httpmock"
func TestValidArticleWithNonZeroID(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	httpmock.RegisterResponder("GET", "https://api.mybiz.com/articles",
		httpmock.NewStringResponder(200, `[{"id": 1, "name": "My Great Article"}]`))
	resp := isArticleValid() //when above url will be called in this function, it should return mocked result in response body

	// get the amount of calls for the registered responder
	fmt.Println("number of times isArticleValid() is called", httpmock.GetTotalCallCount())
	fmt.Println("response from callHTTP is", resp)
	if resp {
		t.Logf("Test sum passed expected %v, got %v", true, resp)
	} else {
		t.Errorf("Test sum passed expected %v, got %v", true, resp)
	}
}

//test http mock using import "github.com/jarcoal/httpmock"
func TestValidArticleWithZeroID(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	httpmock.RegisterResponder("GET", "https://api.mybiz.com/articles",
		httpmock.NewStringResponder(200, `[{"id": 0, "name": "My Great Article"}]`))
	resp := isArticleValid()
	httpmock.GetTotalCallCount()

	// get the amount of calls for the registered responder
	info := httpmock.GetCallCountInfo()
	fmt.Println("number of times GetCallCountInfo() is called", info["GET https://api.mybiz.com/articles"])
	fmt.Println("response from callHTTP is", resp)
	if !resp {
		t.Logf("Test sum passed expected %v, got %v", true, resp)
	} else {
		t.Errorf("Test sum passed expected %v, got %v", true, resp)
	}
}
