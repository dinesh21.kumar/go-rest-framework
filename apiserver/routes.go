package main

import (
	"github.com/gorilla/mux"
)

func newRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/posts", getPosts).Methods("GET")
	router.HandleFunc("/posts", createPost).Methods("POST")
	router.HandleFunc("/welcome", welcomeMessage).Methods("GET")
	router.HandleFunc("/employees", employees).Methods("GET")
	router.HandleFunc("/employees", addEmployee).Methods("POST")
	router.HandleFunc("/employees", removeEmployee).Methods("DELETE")
	router.HandleFunc("/employees", changeEmployeeLastName).Methods("PUT")
	router.HandleFunc("/airlines/status", asyncAirlineStatus).Methods("GET")

	//api versioning example
	api := router.PathPrefix("/api").Subrouter()
	api.HandleFunc("/v1/example", exampleV1Handl)
	api.HandleFunc("/v2/example", exampleV2Handl)
	return router
}
