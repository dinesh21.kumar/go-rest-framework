package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func mulatiplicationTableWithoutGR(n int) {
	for i := 1; i <= 10; i++ {
		fmt.Printf("%d * %d =  %d\n", n, i, n*i)
	}
	time.Sleep(10000 * time.Microsecond)
	fmt.Println()
}

//intensive task without go routines
func taskExecWithoutGR() {
	fmt.Println("running cpu intensive task without Go routines")
	for j := 1; j <= 12; j++ {
		mulatiplicationTableWithoutGR(j)
	}

}

//go routine
func mulatiplicationTableWithGR(n int) {
	for i := 1; i <= 10; i++ {
		fmt.Printf("%d * %d =  %d\n", n, i, n*i)
	}
	time.Sleep(10000 * time.Microsecond)
	fmt.Println()
	wg.Done() //done with go routine
}

//intensive task with go routines
func taskExecWithGR() {
	fmt.Println("running cpu intensive task with Go routines")
	for j := 1; j <= 12; j++ {
		wg.Add(1) //add task to wait group
		go mulatiplicationTableWithGR(j)
	}
	wg.Wait() //wait till go routine is going on

}

func main() {
	//taskExecWithoutGR()
	taskExecWithGR() //run go code in background

}

//command to run
// time go run filename.go
